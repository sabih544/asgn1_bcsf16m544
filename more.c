#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>  
#include <errno.h>   
#include <sys/wait.h> 
#include <sys/ioctl.h>

#define linelen 512

void set_input_mode (void);
void reset_input_mode (void);
int file(FILE *,char *);
int get_input(FILE*,int,int);
int search_string(FILE* ,char *);
int main(int argc, char *argv[]){

set_input_mode ();
	FILE *fp;
	if(argc==1){
		file(stdin,"NULL");
	}
	else{
		int i=0;
		int file_count;
		while(++i < argc){
			fp=fopen(argv[i],"r");
			if(fp==NULL){
				perror("can't open file");
				exit(0);
			}
			file(fp,argv[i]);
			fclose(fp);
		}	
	}
		return EXIT_SUCCESS;

}
	int file(FILE *fp,char *file_name){
 		int ch=0;
  		int lines=0;
  		if (fp == NULL){
  			return 0;
  		}
  		while(!feof(fp))
		{
		  ch = fgetc(fp);
		  if(ch == '\n')
		  {
		    lines++;
		  }
		}
		fseek(fp, 0, SEEK_SET);

		int line_count = 0;
		int t_lcount=0;
		int inp;
		char arr[linelen];
		while (fgets(arr,linelen,fp))
		{
			struct winsize ws;
			ioctl(2,TIOCGWINSZ,&ws);
			int page=ws.ws_row;
			int pagelen=page-1;
			FILE* fp_tty=fopen("/dev/tty","r");
			fputs(arr,stdout);
			line_count++;
			t_lcount++;
			if(line_count >= pagelen)
			{
				inp=get_input(fp_tty,lines,t_lcount);
				if(inp == 0 ){
					printf("\033[1A \033[2K \033[1G");
					break;
				}
				else if(inp == 1)
				{
					struct winsize ws;
					ioctl(2,TIOCGWINSZ,&ws);
					int page=ws.ws_row;
					int new_pagelen=page-1;
					printf("\033[1A \033[2K \033[1G");
					line_count=new_pagelen-1;
				}
				else if(inp == 2)
				{
					printf("\033[1A \033[2K \033[1G");
					line_count=0; 
				}

				else if(inp==3)
				{
					printf("\033[1A \033[2K \033[1G");
					printf("/");
					system("stty -cbreak echo <&2");
					char str[100];
					fgets(str,500,fp_tty);
					int countt;
					fseek(fp, 0, SEEK_SET);
					countt=search_string(fp,str);
					if(countt!=-1)
					{
						int i=0;
						int skip_count=countt-2;
						printf(" Skipping.... \n");
						if(countt>=t_lcount)
						{
							line_count=0;
							fseek(fp, 0, SEEK_SET);
							char arr[200];
							t_lcount=0;
							int t_lcount2=0;
							while(i!=skip_count)
							{
								fgets(arr,512,fp);
								i++;
								t_lcount2++;
							}
							t_lcount=t_lcount2;
							char ar2[200];
							int j=4;
							int n=0;
							while(j<pagelen)
							{
								fgets(ar2,512,fp);
								fputs(ar2,stdout);
								j++;
								t_lcount++;
							}
							line_count=pagelen;
						}
						else{
							int k=0;
							printf("%d\n", t_lcount);
							fseek(fp, 0, SEEK_SET);
							while(k!=t_lcount){
								fgets(arr,512,fp);
								k++;
							}
						}
				}
					else
					{
						printf("Pattern not found \n");
							int k=0;
							printf("%d\n", t_lcount);
							fseek(fp, 0, SEEK_SET);
							while(k!=t_lcount){
								fgets(arr,512,fp);
								k++;
							}
					}
						//system("stty cbreak -echo <&2");
							set_input_mode ();
				}
				else if(inp == 4)
				{
					printf("\033[1A \033[2K \033[1G");                              
					pid_t  f;
					int status;
					f=vfork(); 
					if(f==-1){
						printf("error\n");
					}
					if(f==0){
						execlp("vim","vim",file_name, NULL);
						exit(0);
					}
					else{
						wait(NULL);
					}
			}
				
				else if(inp == 5){
					printf("\033[1A \033[2K \033[1G");
					break;
				}
			}
		}
	}
	int search_string(FILE *fp,char *find)
	{
		int count=0;
		char arr[500];
		int flag=-1;
		while(!feof(fp))
		{
			fgets(arr,500,fp);
			if(strstr(arr,find)){
				flag=0;
				break;
			}
			count++;
		}
		if(flag==0)
			return count;		
		else
			return -1;
	}

	int get_input(FILE *cmdstream,int count,int lcount){
		int per;
		per = (float)lcount / (float)count * 100.0;
		printf("\033[7m --more--(%d%%) \033[m",per);
		char u_input;
		printf("\n");
		u_input=getc(cmdstream);
		if(u_input=='q')
			return 0;
		if(u_input=='\n')
			return 1;
		if(u_input==' ')
			return 2;
		if(u_input=='/')
			return 3;
		if(u_input=='v' || 'V')
			return 4;
		else 
			return 5;
	}


	struct termios saved_attributes;
	void reset_input_mode (void)
	{
  		tcsetattr (STDIN_FILENO, TCSANOW, &saved_attributes);
	}

	void set_input_mode (void)
	{
	  struct termios tattr;
	  char *name;
	  if (!isatty (STDIN_FILENO))
	    {
	      fprintf (stderr, "Not a terminal.\n");
	      exit (EXIT_FAILURE);
	    }
	  tcgetattr (STDIN_FILENO, &saved_attributes);
	  atexit (reset_input_mode);

	  tcgetattr (STDIN_FILENO, &tattr);
	  tattr.c_lflag &= ~(ICANON|ECHO); 
	  tattr.c_cc[VMIN] = 1;
	  tattr.c_cc[VTIME] = 0;
	  tcsetattr (STDIN_FILENO, TCSAFLUSH, &tattr);
	}
